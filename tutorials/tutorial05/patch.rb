class Horse
  include Comparable

  attr_accessor :value

  def initialize(value)
    self.value = value
  end

  def +(other)
    other + value
  end

  def coerce(other)
    [Horse.new(other), self]
  end

  def <=>(other)
    value <=> other.value
  end
end

# zprovoznit (1..horse).to_a udela range
horse = Horse.new(42)
pony = Horse.new(11)

puts horse + 22
puts horse + pony
puts 11 + horse
puts [horse, pony].sum

puts (1..Horse.new(10)).to_a