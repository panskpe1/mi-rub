def caller(that)
  result = that.call
  puts "So I called it and it returned: #{result}"
end

lam = -> { return 'return in lambda' }
proc = proc { return 'return in proc' }

caller lam
caller proc
