def add(f, s = 2)
	f + s
end

def add(f = 2, s)
	f + s
end

def add(f, s = 2, t)
	f + s + t
end

p "s"
p add(3)
p add(3, 1)
p ""
p "t"
p add(1,2,3)
p add(1,3)


def add(first: 10, second: 20)
	first + second
end

p add first: 1, second: 2
p add first: 1
