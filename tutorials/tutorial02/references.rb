def foo(param)
  param = 5
end

victim = 1
foo(victim)
p victim

victim = 'text'
foo(victim)
p victim

victim = {key: :value}
foo(victim)
p victim

def bar(params)
  params[:surprise] = :lol
end

victim = {key: :value}
bar(victim)
p victim

def wat(param)
  param << "YEET"
end

victim = 'text'
wat(victim)
p victim

victim = [:simple, :array]
wat(victim)
p victim

def hmm(params)
  params = 'yeet'
end

victim = 'text'
hmm(victim)
p victim

victim = [:simple, :array]
hmm(victim)
p victim