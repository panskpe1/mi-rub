require 'bundler/setup'
Bundler.require(:default)

# require 'prawn'
# require 'ruby-progressbar'

class Test < Thor
  desc "example", "an example task"
  def example
    puts "I'm a thor task!"
  end

  desc "make_pdf", "creates PDF"
  def make_pdf
    Prawn::Document.generate("hello.pdf") do
      text "Hello World!"
    end
  end

  desc "make_progress", "creates progress bar"
  def make_progress
    progressbar = ProgressBar.create
    50.times { progressbar.increment ; sleep 0.1 }
  end
end



# =============
require 'byebug'

def bad_method
  one = 1
  two = nil
  # byebug
  binding.pry
  one + two
end

bad_method
