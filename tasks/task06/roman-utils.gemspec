Gem::Specification.new do |s|
  s.name        = 'roman-utils'
  s.version     = '0.1.0'
  s.date        = '2019-11-04'
  s.license     = 'MIT'
  s.author      = 'Petr Panský'
  s.email       = 'panskpe1@fit.cvut.cz'
  s.summary     = 'Roman utils'
  s.description = 'CLI that provides to user a set of utils that makes easier conversion between roman and integer numbers.'

  s.files       = Dir['bin/*', 'lib/**/*', '*.gemspec', 'LICENSE*', 'README*']
  s.executables = Dir['bin/*'].map { |f| File.basename(f) }

  s.required_ruby_version = '>= 2.2'

  s.add_development_dependency 'rspec', '~> 3.6'
  s.add_development_dependency 'yard', '~> 0.9'
  s.add_development_dependency 'byebug', '~> 11.0'


end