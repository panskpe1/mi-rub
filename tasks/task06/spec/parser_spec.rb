require_relative '../lib/parser'

describe 'parser' do

  before :each do
    @parser = Parser.new
  end

  it 'should set flat help to true if there is no argument' do
    @parser.parse([])
    expect(@parser.help?).to be(true)
    expect(@parser.integer?).to be(false)
    expect(@parser.roman?).to be(false)
  end

  it 'should set flat help to true if there is a help argument' do
    @parser.parse([])
    expect(@parser.help?).to be(true)
    expect(@parser.integer?).to be(false)
    expect(@parser.roman?).to be(false)
  end

  it 'should print help with all possible parameters' do
    expect { @parser.print_help }.to output(/Usage: roman-utils/).to_stdout
    expect { @parser.print_help }.to output(/-r/).to_stdout
    expect { @parser.print_help }.to output(/-i/).to_stdout
    expect { @parser.print_help }.to output(/-h/).to_stdout
  end

  it 'should read a number value if there is an integer input argument' do
    @parser.parse(%w(-i 123))
    expect(@parser.help?).to be(false)
    expect(@parser.integer?).to be(true)
    expect(@parser.roman?).to be(false)
    expect(@parser.both_input_options?).to be(false)

    expect(@parser.integer).to eq('123')
  end

  it 'should read a number value if there is an roman input argument' do
    @parser.parse(%w(-r XI))
    expect(@parser.help?).to be(false)
    expect(@parser.integer?).to be(false)
    expect(@parser.roman?).to be(true)
    expect(@parser.both_input_options?).to be(false)

    expect(@parser.roman).to eq('XI')
  end

  it 'should read both input options' do
    @parser.parse(%w(-r XI -i 123))
    expect(@parser.help?).to be(false)
    expect(@parser.integer?).to be(true)
    expect(@parser.roman?).to be(true)
    expect(@parser.both_input_options?).to be(true)
  end

end
