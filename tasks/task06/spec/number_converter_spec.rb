require_relative '../lib/number_converter'
require_relative '../lib/invalid_input_format'

describe 'number_converter' do

  before :each do
    @converter = NumberConverter.new
  end

  it 'should raise an error because invalid integer input' do
    expect { @converter.int_to_roman('number') }.to raise_error(InvalidInputFormat)
  end

  it 'should raise an error because nil integer input' do
    expect { @converter.int_to_roman(nil) }.to raise_error(InvalidInputFormat)
  end

  it 'should convert integers to expected roman values' do
    test_int_to_roman('1', 'I')
    test_int_to_roman('10', 'X')
    test_int_to_roman('19', 'XIX')
    test_int_to_roman('1234', 'MCCXXXIV')
    test_int_to_roman('2999', 'MMCMXCIX')
  end

  it 'should raise an error because invalid roman input' do
    expect { @converter.roman_to_int('XIS') }.to raise_error(InvalidInputFormat)
  end

  it 'should raise an error because nil roman input' do
    expect { @converter.roman_to_int(nil) }.to raise_error(InvalidInputFormat)
  end

  it 'should convert roman values to expected integers' do
    test_roman_to_int('I', 1)
    test_roman_to_int('X', 10)
    test_roman_to_int('XIX', 19)
    test_roman_to_int('MCCXXXIV', 1234)
    test_roman_to_int('MMCMXCIX', 2999)
  end

  def test_int_to_roman(int_str_value, expected_roman_value)
    expect(@converter.int_to_roman(int_str_value)).to eq(expected_roman_value)
  end

  def test_roman_to_int(roman_value, expected_int)
    expect(@converter.roman_to_int(roman_value)).to eq(expected_int)
  end

end
