# Class used for throwing invalid input format
class InvalidInputFormat < StandardError

  # Initialize
  # @param [String] msg Message
  def initialize(msg = 'Invalid input format')
    super
  end
end
