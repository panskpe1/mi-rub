require_relative 'invalid_input_format'
# code from:
# https://www.geeksforgeeks.org/converting-decimal-number-lying-between-1-to-3999-to-roman-numerals/
# https://www.geeksforgeeks.org/converting-roman-numerals-decimal-lying-1-3999/
NUM = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000].freeze
SYM = %w[I IV V IX X XL L XC C CD D CM M].freeze

# Converter that allow to convert between roman and integer numbers.
class NumberConverter

  # Converts the roman value to the integer.
  #
  # @param [String] str_value Roman value
  # @return [Integer] Same value as an integer
  def roman_to_int(str_value)
    unless valid_roman?(str_value)
      raise InvalidInputFormat, "Invalid input format of an roman number: '#{str_value}'"
    end

    res = 0
    array = str_value.split('')

    index = 0
    while index < array.size
      s1 = char_to_int(array[index])

      if index + 1 < array.size
        s2 = char_to_int(array[index + 1])

        if s1 >= s2
          res += s1
        else
          res += s2 - s1
          index += 1
        end
      else
        res += s1
      end

      index += 1
    end

    res
  end

  # Converts the integer to the roman value.
  #
  # @param [String] str_value Integer value
  # @return [String] Same value as an roman
  def int_to_roman(str_value)
    unless valid_int?(str_value)
      raise InvalidInputFormat, "Invalid input format of an integer number: '#{str_value}'"
    end

    number = str_value.to_i

    index = 12
    res = ''
    while number.positive?
      div = number / NUM[index]
      number = number % NUM[index]
      while div.positive?
        res += SYM[index]
        div -= 1
      end
      index -= 1
    end
    res
  end

  private

  # Convert the single char in the roman format to the integer value
  #
  # @param [String] char Single character
  # @return [Integer] Roman char's integer representation
  def char_to_int(char)
    index = SYM.index(char)
    NUM[index]
  end

  # Is the number in string in the valid number format?
  #
  # @param [String] str_value Number in string
  # @return [Boolean] Is number valid?
  def valid_int?(str_value)
    str_value.to_i.to_s == str_value
  end

  # Is the roman in string in the valid roman format?
  #
  # @param [String] str_value Roman in string
  # @return [Boolean] Is roman valid?
  def valid_roman?(str_value)
    return false if str_value.nil?

    characters = str_value.split('')
    characters.uniq.each do |char|
      return false unless SYM.include?(char)
    end

    true
  end

end