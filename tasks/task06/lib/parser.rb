require 'optparse'

class Parser
  def initialize
    @opts = {}
    @opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: roman-utils [options]'

      opts.on('-rROMAN', '--roman=ROMAN', 'Roman input') do |value|
        @opts[:roman] = value
      end

      opts.on('-iINTEGER', '--integer=INTEGER', 'Integer input') do |value|
        @opts[:integer] = value
      end

      opts.on('-h', '--help', 'Prints this help') do
        @opts[:help] = true
      end
    end
  end

  def parse(options)
    @opt_parser.parse!(options)
  end

  def help?
    @opts.include?(:help) || @opts.empty?
  end

  def print_help
    puts @opt_parser
  end

  def both_input_options?
    roman? && integer?
  end

  def roman?
    @opts.include?(:roman)
  end

  def integer?
    @opts.include?(:integer)
  end

  def roman
    raise RuntimeError 'Missing roman input type' unless roman?
    @opts[:roman]
  end

  def integer
    raise RuntimeError 'Missing integer input type' unless integer?
    @opts[:integer]
  end

end
