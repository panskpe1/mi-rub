require_relative 'number_converter'
require_relative 'parser'

# Roman utils module
module RomanUtils

  # Client class
  class CLI

    # Initialize CLI with arguments that a user given to the application.
    # @param [{}] options Arguments
    def initialize(options = {})
      parser = Parser.new

      # Parses given arguments from the console
      begin
        parser.parse(options)
      rescue OptionParser::MissingArgument, OptionParser::InvalidOption => e
        print_help_with_message(parser, e.to_s)
      end

      if parser.help?
        print_help_with_message(parser)
      elsif parser.both_input_options?
        print_help_with_message(parser, 'Use only one param from integer and roman input type!')
      end

      converter = NumberConverter.new

      begin
        if parser.roman?
          value = converter.roman_to_int(parser.roman)
          puts "Integer value: #{value}"
        elsif parser.integer?
          value = converter.int_to_roman(parser.integer)
          puts "Roman value: #{value}"
        end
      rescue InvalidInputFormat => e
        print_help_with_message(parser, e.to_s)
      end
    end

    # Prints help and if there is some message, prints it too.
    # After an printer help, exit the application.
    #
    # @param [Parser] parser Parser
    # @param [String] message Message to print. Default is nil.
    def print_help_with_message(parser, message = nil)
      puts message unless message.nil?
      parser.print_help
      exit
    end

  end
end
