# roman-utils
The command line application that provides to user a set of utils that makes easier conversion between roman and integer numbers.

To run an application:
```
ruby roman-utils.rb 
```
or
```
./roman-utils.rb
```
The file *roman-utils.rb* is there only for local testing. If it should be the 'clear' gem, there wouldn't be this file.
### Arguments
*  `-h` / `--help` - Shows help.
*  `-i 5` / `--integer 5` - Converts an integer to a roman format.
*  `-r V` / `--roman V` - Converts a roman to an integer format.

### Development / testing
It's needed to run `bundle install` to install gems from *Gemfile* file.

### Documentation
There is used documentation tool **YARD**.
 
To generate the documentation:
```
bundle install
yardoc '*.rb' README
```

### Build and install gem
```
gem build roman-utils.gemspec
gem install roman-utils-0.1.0.gem
```
Now you can use it as the system command `roman-utils`