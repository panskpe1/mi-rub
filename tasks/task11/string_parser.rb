# frozen_string_literal: true

require 'grid'
require 'cell'
# Parse string for 9x9 sudoku game
class StringParser

  REGEX_INPUT = /\A[0-9\.]+\Z/i
  STR_LEN = 81

  # Static methods will follow
  class << self
    # Return true if passed object
    # is supported by this loader
    def supports?(arg)
      return false if arg.nil? || arg.to_s.length != STR_LEN

      arg.to_s.match?(REGEX_INPUT)
    end

    # Return Grid object when finished
    # parsing
    def load(arg)
      dim = 9
      grid = Grid.new(dim)

      arg.split('').each_index do |char, index|
        num = char.to_i
        row = (num / dim).to_i
        col = num % dim
        grid[row][col] = char.to_i
      end

      grid
    end
  end
end
