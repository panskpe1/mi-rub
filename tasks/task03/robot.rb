require 'set'
require_relative 'arm'
require_relative 'speech'

# Robot representation
class Robot
  include Speech
  include Comparable

  attr_reader :name, :arms

  def initialize(name = 'RoboCop')
    @name = name
    @arms = []
  end

  def add_arms(*arms)
    @arms.concat(arms)
  end

  def introduce
    types_set = @arms.map(&:type).to_set
    types_str = types_set.map(&:to_s).sort.join(', ')

    speak("Hi, my name is #{name}, I have #{@arms.length} arms with types : #{types_str}")
  end

  def <=>(other)
    score <=> other.score
  end

  def score
    @arms.map(&:score).sum
  end

  def speak(text)
    puts text
  end
end
