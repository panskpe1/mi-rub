# Module A
module A
  def hi
    p 'Hi. It works!'
  end
end

# Module B
module B
  def hello
    p 'Hello. It works!'
  end
end

# Module C
module C
  def hello2
    p 'Hello2'
  end
end

# Class Foo
class Foo

  # Monkey patch, ale pouze nad tridni metodou "extend" tridy Foo.
  # Neovlivni tak pouzivani v ostatnich tridach, pouze ve Foo.
  # V metode misto extend metody zavolame extend metodu, ktera nam prida metody na objektu Foo
  def self.extend(*mod)
    self.include(*mod)
  end

  extend A, B
  extend C

end

foo = Foo.new
foo.hi
foo.hello
foo.hello2

p 'Ancestors order: ', Foo.ancestors