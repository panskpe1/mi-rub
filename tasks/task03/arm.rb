# Arm representation
class Arm
  attr_reader :type, :length

  TYPES = { poker: 1, slasher: 2, grabber: 3 }.freeze
  DEFAULT_LEN = 1

  def initialize(type, length)
    raise ArgumentError, 'Invalid arm type' unless TYPES.keys.include?(type)

    @type = type
    @length = length
  end

  def self.poker(length = DEFAULT_LEN)
    Arm.new(:poker, length)
  end

  def self.slasher(length = DEFAULT_LEN)
    Arm.new(:slasher, length)
  end

  def self.grabber(length = DEFAULT_LEN)
    Arm.new(:grabber, length)
  end

  def to_s
    '{type: ' + type.to_s + ', length: ' + length.to_s + '}'
  end

  def score
    TYPES[type]
  end
end
