# Speech module
module Speech

  def shout(text)
    speak(text.upcase)
  end

  def whisper(text)
    speak(text.downcase)
  end

  def annoy(text, number = rand(1..10))
    number.times do
      fixed_size = rand(1..5)
      parts = text.split(/(\w{#{fixed_size}})/)
      text_to_print = parts.shuffle.join

      speak(text_to_print)
    end
  end
end