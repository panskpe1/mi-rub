require 'test/unit'
require_relative '../speech'

# Test cases
class SpeechTest < Test::Unit::TestCase

  def setup
    @target = TestTarget.new
  end

  def test_speak
    assert_equal('Text', @target.speak('Text'), 'Wrong speak implementation')
  end

  def test_shout
    assert_equal('TEXT', @target.shout('Text'), 'Wrong upcase text')
    assert_equal('TEXT', @target.shout('TeXT'), 'Wrong upcase text')
  end

  def test_whisper
    assert_equal('text', @target.whisper('Text'), 'Wrong upcase text')
    assert_equal('text', @target.whisper('TeXT'), 'Wrong upcase text')
  end

  def test_annoy
    @target.annoy('Text k upravam', 4)
    assert_equal(4, @target.text_lines.length)
    puts @target.text_lines.join

    10.times do
      @target.text_lines = []
      @target.annoy('Text k upravam je tady', 4)
      assert_true(@target.text_lines.length.between?(1, 10), 'wrong number')
    end
  end

end

# Test class
class TestTarget
  attr_accessor :text_lines
  include Speech

  def speak(text)
    @text_lines = [] if @text_lines.nil?
    @text_lines << text

    text
  end
end


