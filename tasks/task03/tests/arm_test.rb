require 'test/unit'
require_relative '../arm'

# Test cases
class ArmTest < Test::Unit::TestCase

  EXPECTED_DEFAULT_LEN = 10

  def test_poker
    arm = Arm.poker(3)
    assert_equal(3, arm.length, 'wrong length')
    assert_equal(:poker, arm.type, 'wrong type')
    assert_equal(1, arm.score, 'wrong score')
  end

  def test_slasher
    arm = Arm.slasher(4)
    assert_equal(4, arm.length, 'wrong length')
    assert_equal(:slasher, arm.type, 'wrong type')
    assert_equal(2, arm.score, 'wrong score')
  end

  def test_grabber
    arm = Arm.grabber(5)
    assert_equal(5, arm.length, 'wrong length')
    assert_equal(:grabber, arm.type, 'wrong type')
    assert_equal(3, arm.score, 'wrong score')
  end
end


