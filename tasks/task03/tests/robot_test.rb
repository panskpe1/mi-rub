require 'test/unit'
require_relative '../arm'
require_relative '../robot'

# Test cases
class RobotTest < Test::Unit::TestCase

  def setup
    @robot = Robot.new('Joker')
  end

  def test_default_initialize
    robot = Robot.new
    assert_equal('RoboCop', robot.name, 'wrong name')
  end

  def test_add_arms
    expected_arms = [Arm.grabber(2)]
    @robot.add_arms(expected_arms[0])
    out, = capture_output do
      @robot.introduce
    end
    assert_equal("Hi, my name is Joker, I have 1 arms with types : grabber\n", out,
                 'wrong introduce')
    assert_equal(expected_arms, @robot.arms, 'wrong arms')

    expected_arms << Arm.slasher(3)
    @robot.add_arms(expected_arms[1])
    assert_equal(expected_arms, @robot.arms, 'wrong arms')

    expected_arms << Arm.poker(2) << Arm.slasher(4) << Arm.poker(6)
    puts expected_arms
    @robot.add_arms(expected_arms[2], expected_arms[3], expected_arms[4])
    assert_equal(expected_arms, @robot.arms, 'wrong arms')

    out, = capture_output do
      @robot.introduce
    end
    assert_equal("Hi, my name is Joker, I have 5 arms with types : grabber, poker, slasher\n", out,
                 'wrong introduce ')
  end

  def test_score
    assert_equal(0, @robot.score, 'wrong score')
    @robot.add_arms(Arm.poker, Arm.poker)
    assert_equal(2, @robot.score, 'wrong score')
    @robot.add_arms(Arm.slasher, Arm.grabber)
    assert_equal(7, @robot.score, 'wrong score')

  end

  def test_comparable
    robot1 = Robot.new
    robot1.add_arms(Arm.poker, Arm.slasher, Arm.grabber) # score 6

    robot2 = Robot.new
    robot2.add_arms(Arm.slasher) # score 2

    robot3 = Robot.new
    robot3.add_arms(Arm.grabber) # score 3

    assert_true(robot1 == robot1.clone, 'invalid ==')

    assert_true(robot1 >= robot1.clone, 'invalid >=')
    assert_true(robot1 >= robot2, 'invalid >=')

    assert_true(robot1 <= robot1.clone, 'invalid <=')
    assert_true(robot2 <= robot1, 'invalid >=')

    assert_true(robot1 > robot2, 'invalid >')
    assert_false(robot1 < robot2, 'invalid <')


    assert_true(robot3.between?(robot2, robot1), 'invalid between?')
  end

  def test_put_arms
    @robot.add_arms(Arm.poker(4), Arm.slasher(5), Arm.grabber(6)) # score 6

    out, = capture_output do
      puts @robot.arms
    end

    assert_equal(
        "{type: poker, length: 4}\n{type: slasher, length: 5}\n{type: grabber, length: 6}\n",
        out,
        'wrong ouput')

  end
end
