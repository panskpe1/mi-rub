# Několik způsobů, jak zavolat metodu
#
hello_method = Foo.new.method :hello
hello_method.call
hello_method.to_proc.call


hello_method x = proc { Foo.new.hello }
hello_method.call
