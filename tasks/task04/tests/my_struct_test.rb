require 'test/unit'
require_relative '../my_struct'


# Test cases
class MyStructTest < Test::Unit::TestCase

  def test_init_1
    person = MyStruct.new
    person.name = 'John Smith'
    person.age = 70

    assert_equal('John Smith', person.name)
    assert_equal(70, person.age)
    assert_nil(person.address)
  end

  def test_init_2
    australia = MyStruct.new(:country => 'Australia', :capital => 'Canberra')
    assert_equal('Australia', australia.country)
    assert_equal('Canberra', australia.capital)
  end

  def test_init_3
    measurements = MyStruct.new('length (in inches)' => 24)
    assert_equal(24, measurements.send('length (in inches)'))

    message = MyStruct.new(:queued? => true)
    assert_true(message.queued?)
    message.send('queued?=', false)
    assert_false(message.queued?)
  end

  def test_delete
    person = MyStruct.new(name: 'John', age: 70, pension: 300)
    person.delete_field('age')
    assert_nil(person.age)

    pet = MyStruct.new(:name => 'Rowdy', :owner => 'John Smith')
    pet.delete_field(:owner)
    assert_nil(pet.owner)
  end

  def test_to_h
    measurements = MyStruct.new("length (in inches)" => 24)
    assert_equal({:"length (in inches)" => 24}, measurements.to_h)

    data = MyStruct.new("country" => "Australia", :capital => "Canberra")
    assert_equal({:country => "Australia", :capital => "Canberra"}, data.to_h)
  end

end