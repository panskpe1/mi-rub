class MyStruct

  def initialize(opt = {})
    @hash = {}
    opt.each do |key, value|
      @hash.store(key.to_sym, value)
    end
  end

  def method_missing(method_name, *args, &block)
    method_name_str = method_name.to_s
    if method_name_str.end_with?('=') && args.length == 1
      key_sym = method_name_str[0..-2].to_sym
      @hash.store(key_sym, args[0])
    elsif args.length.zero?
      @hash[method_name]
    else
      super
    end
  end

  def delete_field(name)
    @hash.delete(name.to_sym)
  end

  def to_h
    @hash
  end

end