# ePublication

Rails version: `6.0.1`

Ruby version: `2.6.3`

### Production
Production url: https://epublication.herokuapp.com/


To run locally it's needed: PostgreSQL database


### Development
How to run app:
```
bundle install
rails db:create
rails db:migrate 
```
If you want to have sample data before the start server, run:
```
rails db:seed
```
This command fill the database with predefined data.

And then:
```
rails s
```
to start the server that runs at the address: `http://localhost:3000`.


### Testing
For testing, it's used the sqlite3 database, so you don't need PostgreSQL.

If you want to test implementation of controllers and models, run the command: 
```
rails test
```



