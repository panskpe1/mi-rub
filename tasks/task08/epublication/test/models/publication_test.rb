require 'test_helper'

class PublicationTest < ActiveSupport::TestCase

  setup do
    @faculty1 = faculties(:one)
    @faculty2 = faculties(:two)
    @person1 = people(:one)
    @person2 = people(:two)
    @publication1 = publications(:one)
    @publication2 = publications(:two)
    @publication = Publication.new(name: 'Publication', published_at: Time.zone.now, abstract: 'Abstract', person: @person1)
  end

  test 'should get all publications' do
    publications = Publication.all.order(id: :desc)
    assert_equal 2, publications.count
    assert_equal publications.first, @publication1
    assert_equal publications.second, @publication2
  end

  test 'should be valid publication' do
    assert @publication.valid?

    @publication.abstract = nil
    assert @publication.valid?

    @publication.published_at = nil
    assert @publication.valid?
  end

  test 'should not be valid publication due to invalid name' do
    @publication.name = nil
    assert_not @publication.valid?

    @publication.name = '          '
    assert_not @publication.valid?
  end

  test 'should not be valid publication due to invalid person' do
    @publication.person = nil
    assert_not @publication.valid?

    @publication.person_id = 100
    assert_not @publication.valid?
  end

  test 'should get faculty' do
    assert_equal @person1, @publication.person
    assert_equal @faculty1, @publication.faculty

    @publication.person = @person2
    assert_equal @person2, @publication.person
    assert_equal @faculty2, @publication.faculty
  end

  test 'published should be tested' do
    @publication.published_at = Time.zone.now
    assert @publication.published?

    @publication.published_at = nil
    assert_not @publication.published?
  end
end
