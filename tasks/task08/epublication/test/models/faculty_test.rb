require 'test_helper'

class FacultyTest < ActiveSupport::TestCase
  setup do
    @faculty1 = faculties(:one)
    @faculty2 = faculties(:two)
    @faculty3 = faculties(:three)
  end

  test 'should get all faculties' do
    faculties = Faculty.all.order(id: :desc)
    assert_equal 3, faculties.count
    assert_equal faculties.first, @faculty1
    assert_equal faculties.second, @faculty2
    assert_equal faculties.third, @faculty3
  end

  test 'should be valid faculty' do
    faculty = Faculty.new(name: 'New faculty', code: 'code')
    assert faculty.valid?
  end

  test 'should not be valid faculty due to invalid code' do
    faculty = Faculty.new(name: 'New faculty')
    assert_not faculty.valid?

    faculty = Faculty.new(name: 'New faculty', code: '     ')
    assert_not faculty.valid?
  end

  test 'should not be valid faculty due to invalid name' do
    faculty = Faculty.new(code: 'code')
    assert_not faculty.valid?

    faculty = Faculty.new(code: 'code', name: '   ')
    assert_not faculty.valid?
  end

  test 'should not be valid faculty due to not uniq code' do
    faculty = Faculty.new(name: 'Name', code: 'code1')
    assert_not faculty.valid?
  end
end
