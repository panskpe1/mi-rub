require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  setup do
    @faculty = faculties(:one)
    @person1 = people(:one)
    @person2 = people(:two)
    @person = Person.new(fullname: 'Fullname', username: 'username', email: 'email@email.cz', faculty: @faculty)
  end

  test 'should get all persons' do
    persons = Person.all.order(id: :desc)
    assert_equal 2, persons.count
    assert_equal persons.first, @person1
    assert_equal persons.second, @person2
  end

  test 'should be valid person' do
    assert @person.valid?
  end

  test 'should not be validperson due to invalid fullname' do
    @person.fullname = nil
    assert_not @person.valid?

    @person.fullname = '          '
    assert_not @person.valid?
  end

  test 'should not be valid person due to invalid email' do
    @person.email = nil
    assert_not @person.valid?

    @person.email = 'email'
    assert_not @person.valid?

    @person.email = 'email@asdasd'
    assert_not @person.valid?
  end

  test 'should not be validperson due to not unique email' do
    @person.email = @person1.email
    assert_not @person.valid?
  end

  test 'should not be validperson due to invalid username' do
    @person.username = nil
    assert_not @person.valid?

    @person.username = '    '
    assert_not @person.valid?
  end

  test 'should not be validperson due to not unique username' do
    @person.username = @person1.username
    assert_not @person.valid?
  end

  test 'should not be validperson due to invalid faculty' do
    @person.faculty = nil
    assert_not @person.valid?

    @person.faculty_id = 1000
    assert_not @person.valid?
  end

  test 'should get publications' do
    assert_equal 2, @person1.publications.count
    assert @person2.publications.empty?
  end
end
