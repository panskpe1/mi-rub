class CreateFaculties < ActiveRecord::Migration[6.0]
  def change
    create_table :faculties do |t|
      t.string :name, null: false
      t.string :code, null: false, unique: true
      t.text :note

      t.timestamps
    end
  end
end
