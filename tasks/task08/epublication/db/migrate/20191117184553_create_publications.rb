class CreatePublications < ActiveRecord::Migration[6.0]
  def change
    create_table :publications do |t|
      t.string :name, null: false
      t.datetime :published_at
      t.text :abstract
      t.references :person, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
  end
end
