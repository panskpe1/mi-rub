class CreatePeople < ActiveRecord::Migration[6.0]
  def change
    create_table :people do |t|
      t.string :fullname, null: false
      t.string :email, null: false, unique: true
      t.string :username, null: false, unique: true
      t.references :faculty, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
  end
end
