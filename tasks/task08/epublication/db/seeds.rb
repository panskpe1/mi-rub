# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
fit = Faculty.create(name: 'Faculty of Information Technology', code: 'FIT')
fel = Faculty.create(name: 'Faculty of Electrical Engineering', code: 'FEL')
Faculty.create(name: 'Faculty of Biomedical Engineering', code: 'FBMI')
Faculty.create(name: 'Faculty of Civil Engineering', code: 'FSV')

tonda = Person.create(email: 'tonda.blanik@fit.cvut.cz', fullname: 'Tonda Blanik', username: 'tonblan1', faculty: fit)
honza = Person.create(email: 'honza.karafiat@fit.cvut.cz', fullname: 'Honza Karafiat', username: 'karaf1', faculty: fit)
Person.create(email: 'karel.novak@fit.cvut.cz', fullname: 'Karel Novák', username: 'novakk2', faculty: fel)

Publication.create(name: 'Publikace č.1', abstract: 'Abstralt 1. publikace', person: tonda, published_at: Time.zone.now)
Publication.create(name: 'Publikace č.2', abstract: 'Abstralt 2. publikace', person: honza)
