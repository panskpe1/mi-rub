# Model represents persons
class Person < ApplicationRecord
  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i.freeze

  belongs_to :faculty
  has_many :publications

  validates :fullname, :username, :faculty_id, presence: true
  validates :username, :email, uniqueness: { case_sensitive: false }
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: EMAIL_REGEX }

  # Returns the fullname with the email in the format "fullname <email>"
  #
  # @return [String]
  def name_email
    "#{fullname} <#{email}>"
  end
end
