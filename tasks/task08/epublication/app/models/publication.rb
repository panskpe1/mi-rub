# Model represents publications
class Publication < ApplicationRecord
  belongs_to :person
  validates :name, :person_id, presence: true

  # Returns the publication's faculty.
  # The faculty is found according to the owner of the publication.
  #
  # @return [Faculty]
  def faculty
    person.faculty
  end

  # Is the publication published?
  # If the date is nil => not published yet
  # If the date is not nil => published
  #
  # @return [Boolean]
  def published?
    !published_at.blank?
  end
end
