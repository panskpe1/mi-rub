json.extract! publication, :id, :name, :published_at, :abstract, :person_id, :created_at, :updated_at
json.url publication_url(publication, format: :json)
