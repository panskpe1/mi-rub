# Helper for publications views
module PublicationsHelper
  def selectable_people
    Person.all.map { |f| [f.name_email, f.id] }
  end

  def icon_published(published)
    icon_type = 'times-circle'
    class_name = 'not-published'

    if published
      icon_type = 'check-circle'
      class_name = 'published'
    end

    fa_icon(icon_type, class: "#{class_name} fa-2x")
  end

  def published_date_or_not(published_at)
    published_at.nil? ? 'Not yet' : human_date(published_at)
  end
end
