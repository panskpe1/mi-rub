# Helper for people views
module PeopleHelper
  def selectable_faculties
    Faculty.all.map { |f| [f.name, f.id] }
  end
end
