# Helper for all views
module ApplicationHelper
  DATE_FORMAT = '%d.%m.%Y'.freeze

  def human_boolean(boolean)
    boolean ? 'Yes' : 'No'
  end

  def human_date(date)
    if date.nil?
      ''
    else
      date.strftime(DATE_FORMAT)
    end
  end

  def inline_errors(model, model_attribute)
    result = ''
    if model.errors[model_attribute].any?
      model.errors[model_attribute].each do |message|
        result += "<li>#{message}</li>"
      end
    end
    "<ul>#{result}</ul>".html_safe
  end
end
