require 'faraday'
require_relative 'csv_parser'
require_relative '../date_formatter'

# Bank api
class BankApi
  # Date format: DD.MM.RRRR
  API_ENDPOINT = 'https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt'.freeze

  def initialize
    @api = Faraday.new
    @csv_parser = CsvParser.new
  end

  # Gets all exchanges rates
  def exchange_rates(date)
    date_str = DateFormatter.format_date(date)
    response = @api.get(API_ENDPOINT, date: date_str)

    raise 'Cannot get data for exchanges!' unless response.success?

    content = response.body
    @csv_parser.parse(content)
  end
end