require 'csv'
require 'byebug'
require_relative 'exchange_rate'
require_relative 'exchange_rates'
require_relative '../date_formatter'

# Csv parser
class CsvParser
  DELIMITER = '|'.freeze

  def parse(content)
    rate_date = parse_rate_date(content)
    data = remove_first_2_lines(content)
    rates = CSV.parse(data, col_sep: DELIMITER).map do |row|
      ExchangeRate.new(row[0], row[1], row[2], row[3], parse_number(row[4]))
    end

    ExchangeRates.new(rates, rate_date)
  end

  private

  def remove_first_2_lines(content)
    content.lines[2..-1].join('\n')
  end

  def parse_number(number_str)
    number_str.gsub(',', '.').to_f
  end

  def parse_rate_date(content)
    date_str = content.lines[0].split(' ')[0]
    DateFormatter.parse_date(date_str)
  end
end