require 'byebug'
class ExchangeRates

  attr_reader :exchange_rate_date, :exchange_rates

  CZK = 'CZK'

  def initialize(exchange_rates_array, exchange_rate_date)
    @exchange_rate_date = exchange_rate_date
    @exchange_rates = rates_array_to_hash(exchange_rates_array)
  end

  def know_currency?(code)
    @exchange_rates.include?(code) || code == CZK
  end

  private

  # Convert exchange rate arrays to the hash, where the key is the target currency
  def rates_array_to_hash(exchange_rates)
    exchange_rates.map { |x| [x.code, x] }.to_h
  end
end