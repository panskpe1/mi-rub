# Exchange rate
class ExchangeRate
  attr_reader :country, :currency, :code, :rate

  def initialize(country, currency, amount, code, rate)
    @country = country
    @currency = currency
    @code = code
    @rate = rate.to_f / amount.to_i
  end
end