# Class responsible for formatting and parsing dates
class DateFormatter

  DATE_FORMAT = '%d.%m.%Y'.freeze

  def self.parse_date(date_str)
    Date.strptime(date_str, DATE_FORMAT)
  end

  def self.format_date(date)
    date.strftime(DATE_FORMAT)
  end
end