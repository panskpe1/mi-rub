class Exchange

  attr_reader :amount, :exchange_date

  def initialize(amount, exchange_date)
    @amount = amount
    @exchange_date = exchange_date
  end
end