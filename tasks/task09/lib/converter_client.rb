require_relative 'parser'
require_relative 'currency_converter'
require_relative 'date_formatter'
require_relative 'error/invalid_input_format'

# Currency converter module
module ConverterClient

  # Client class
  class CLI
    # Initialize CLI with arguments that a user given to the application.
    # @param [Object] options
    def initialize(options = {})
      converter = CurrencyConverter.new
      parser = Parser.new

      # Parses given arguments from the console
      begin
        parser.parse(options)
      rescue OptionParser::MissingArgument, OptionParser::InvalidOption, MissingRequiredArgument => e
        print_help_with_message(parser, e.to_s)
      end

      if parser.help?
        print_help_with_message(parser)
        return
      end

      begin
        today = Time.now
        raise InvalidInputFormat, "Invalid amount format: '#{parser.amount}'" unless valid_int?(parser.amount)
        result = converter.convert(parser.from_currency, parser.to_currency, parser.amount.to_i, today)
        puts "#{parser.amount} #{parser.from_currency} -> #{parser.to_currency}, date = #{DateFormatter.format_date(result.exchange_date)}"
        puts("%.2f\n" % result.amount)
      rescue RuntimeError, InvalidInputFormat => e
        print_help_with_message(parser, e.to_s)
      end
    end

    private

    # Prints help and if there is some message, prints it too.
    # After an printer help, exit the application.
    #
    # @param [Parser] parser Parser
    # @param [String] message Message to print. Default is nil.
    def print_help_with_message(parser, message = nil)
      puts message unless message.nil?
      parser.print_help
      exit
    end

    def valid_int?(str_value)
      str_value.to_i.to_s == str_value
    end

  end
end
