require_relative 'api/bank_api'
require_relative 'exchange'

class CurrencyConverter

  CZK = 'CZK'

  def convert(from_currency, to_currency, amount, date)
    api = BankApi.new
    raise "Amount must be > 0: '#{amount}'" unless amount.positive?

    # 'not implemented'
    exchange_rates = api.exchange_rates(date)
    raise "Unknown currency '#{from_currency}'" unless exchange_rates.know_currency?(from_currency)
    raise "Unknown currency '#{to_currency}'" unless exchange_rates.know_currency?(to_currency)

    current_exchange_rate = exchange_rate_for(from_currency, to_currency, exchange_rates.exchange_rates)

    new_amount = amount * current_exchange_rate
    Exchange.new(new_amount.round(3), exchange_rates.exchange_rate_date)
  end

  private

  # exchange_rates - hash
  def exchange_rate_for(from_currency, to_currency, exchange_rates)
    return 1 if from_currency == to_currency

    if from_currency == CZK
      1 / exchange_rates[to_currency].rate
    elsif to_currency == CZK
      exchange_rates[from_currency].rate
    else
      rate1 = exchange_rate_for(from_currency, CZK, exchange_rates)
      rate2 = exchange_rate_for(CZK, to_currency, exchange_rates)
      rate1 * rate2
    end
  end
end