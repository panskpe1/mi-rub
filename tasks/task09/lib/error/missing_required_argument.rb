# Class used for throwing missing required argument
class MissingRequiredArgument < StandardError
  SEPARATOR = ', '.freeze

  # Initialize
  # @param [Array] argument_names Array of argument's names
  def initialize(argument_names)
    super("Missing required arguments: #{argument_names.join(SEPARATOR)}")
  end
end
