require 'optparse'
require_relative 'error/missing_required_argument'

# Class responsible for paring arguments from the command-line
class Parser
  # Initializes options and the parser object
  def initialize
    @opts = {}
    @opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: currency-converter [options]'

      opts.on('-fFROM', '--from=FROM', 'Source currency') do |value|
        @opts[:from] = value.upcase
      end

      opts.on('-tTO', '--to=TO', 'Target currency') do |value|
        @opts[:to] = value.upcase
      end

      opts.on('-mAMOUNT', '--amount=AMOUNT', 'Amount in the source currency') do |value|
        @opts[:amount] = value
      end

      opts.on('-h', '--help', 'Prints this help') do
        @opts[:help] = true
      end
    end
  end

  # Parses given options and validate that required arguments don't miss
  def parse(options)
    @opt_parser.parse!(options)

    self.validate_mandatory_args unless help?
  end

  # Was in options enforced to show help?
  def help?
    @opts.include?(:help) || @opts.empty?
  end

  # Prints help into the standard output
  def print_help
    puts 'Example usage: currency-converter -f CZK -t USD -a 10000'
    puts @opt_parser
  end

  def validate_mandatory_args
    missing_arguments = []
    missing_arguments << 'from' unless @opts.include?(:from)
    missing_arguments << 'to' unless @opts.include?(:to)
    missing_arguments << 'amount' unless @opts.include?(:amount)

    raise MissingRequiredArgument, missing_arguments if missing_arguments.any?
  end

  def from_currency
    @opts[:from]
  end

  def to_currency
    @opts[:to]
  end

  def amount
    @opts[:amount]
  end
end
