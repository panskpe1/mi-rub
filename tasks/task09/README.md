# currency-converter
The command line application that provides to user a currency converter from a set of currencies.  

To run an application:
```
ruby currency-converter.rb 
```
or
```
./currency-converter.rb
```

### Arguments
*  `-h` / `--help` - Shows help.
*  `-f CZK` / `--from CZK` - Source currency
*  `-t CZK` / `--to CZK` - Target currency
*  `-a 1000` / `--amount 1000` - Amount in the source currency

All arguments instead of the argument `-h` are required!

Example usage:
```
ruby currency-converter -f CZK -t USD -a 10000
```