require_relative '../lib/currency_converter'
require_relative '../lib/error/invalid_input_format'

describe 'currency converter' do

  FIXED_DATE = Date.new(2019, 12, 1)
  EXPECTED_DATE = Date.new(2019, 11, 29)

  # TODO: Write tests

  before :each do
    @converter = CurrencyConverter.new
  end

  it 'should convert CZK to CZK without change' do
    result = @converter.convert('CZK', 'CZK', 100, FIXED_DATE)

    expect(result.amount).to eq(100.00)
    expect(result.exchange_date).to eq(EXPECTED_DATE)
  end

  it 'should convert CZK to EUR without change' do
    result = @converter.convert('CZK', 'EUR', 100, FIXED_DATE)

    expect(result.amount).to eq(3.919)
    expect(result.exchange_date).to eq(EXPECTED_DATE)
  end

  it 'should convert EUR to CZK without change' do
    result = @converter.convert('EUR', 'CZK', 10, FIXED_DATE)

    expect(result.amount).to eq(255.15)
    expect(result.exchange_date).to eq(EXPECTED_DATE)
  end

  it 'should convert EUR to USD without change' do
    result = @converter.convert('EUR', 'USD', 10, FIXED_DATE)

    expect(result.amount).to eq(10.982)
    expect(result.exchange_date).to eq(EXPECTED_DATE)
  end

  # Indonesie|rupie|1000|IDR|1,645
  it 'should convert CZK to IDR where IDR has the default amount 1000' do
    result = @converter.convert('CZK', 'IDR', 10, FIXED_DATE)

    expect(result.amount).to eq(6071.645)
    expect(result.exchange_date).to eq(EXPECTED_DATE)
  end
end
