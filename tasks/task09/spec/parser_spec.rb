require_relative '../lib/parser'
require_relative '../lib/error/missing_required_argument'

describe 'parser' do

  before :each do
    # @parser = Parser.new
  end

  # it 'should set flag help to true if there is no argument' do
    # @parser.parse([])
    # expect(@parser.help?).to be(true)
  # end
  #
  # it 'should set flat help to true if there is a help argument' do
  #   @parser.parse(['-h'])
  #   expect(@parser.help?).to be(true)
  # end
  #
  # it 'should print help with all possible parameters' do
  #   expect { @parser.print_help }.to output(/Usage: currency-converter/).to_stdout
  #   expect { @parser.print_help }.to output(/-f/).to_stdout
  #   expect { @parser.print_help }.to output(/-t/).to_stdout
  #   expect { @parser.print_help }.to output(/-a/).to_stdout
  # end
  #
  # it 'should read all arguments' do
  #   @parser.parse(%w(-f CZK -t USD -a 1234))
  #   expect(@parser.help?).to be(false)
  #   expect(@parser.from_currency).to eq('CZK')
  #   expect(@parser.to_currency).to eq('USD')
  #   expect(@parser.amount).to eq('1234')
  # end
  #
  # it 'should detect that there are some missing mandatory arguments' do
  #   expect { @parser.parse(%w(-f CZK)) }.to raise_error(MissingRequiredArgument)
  #   expect(@parser.help?).to be(false)
  #   expect(@parser.from_currency).to eq('CZK')
  # end

end
