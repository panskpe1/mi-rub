require 'test/unit'
require_relative '../roman'
require_relative '../integer_patch'
require_relative '../string_patch'


# Test cases
class RomanTest < Test::Unit::TestCase

  using IntegerPatch
  using StringPatch

  def test_init_number
    assert_raises do
      Roman.new(0)
    end

    Roman.new(1)
    Roman.new(3000)
    assert_raises do
      Roman.new(3001)
    end
  end

  def test_init_string
    assert_raises do
      Roman.new('asd')
    end

    Roman.new('I')
    Roman.new('MMM')
    assert_raises do
      Roman.new('MMMI')
    end
  end

  def test_add
    assert_equal(250, Roman.new(50) + 200)
    assert_equal(250, 50 + Roman.new(200))
  end

  def test_subtract
    assert_equal(150, Roman.new(200) - 50)
    assert_equal(150, 200 - Roman.new(50))
  end

  def test_multiply
    assert_equal(1000, Roman.new(5) * 200)
    assert_equal(1000, 5 * Roman.new(200))
  end

  def test_divide
    assert_equal(20, Roman.new(200) / 10)
    assert_equal(20, 200 / Roman.new(10))
  end

  def test_eql
    assert_true(Roman.new(10).eql?(10))
    assert_true(10.eql?(Roman.new(10)))
  end

  def test_use_as_int_param
    assert_equal((1..10).to_a, (1..100).first(Roman.new(10)))
  end

  def test_to_s
    assert_equal('I', Roman.new(1).to_s)
    assert_equal('IV', Roman.new(4).to_s)
    assert_equal('V', Roman.new(5).to_s)
    assert_equal('VI', Roman.new(6).to_s)
    assert_equal('L', Roman.new(50).to_s)
    assert_equal('C', Roman.new(100).to_s)
    assert_equal('CL', Roman.new(150).to_s)
    assert_equal('DCC', Roman.new(700).to_s)
    assert_equal('M', Roman.new(1000).to_s)
    assert_equal('MMM', Roman.new(3000).to_s)
  end

  def test_int_to_rom
    assert_equal(Roman.new(5), 5.to_rom)
  end

  def test_string_to_rom
    assert_equal(Roman.new(5), 'V'.to_rom)
  end

end