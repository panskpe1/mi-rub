require_relative './roman'

# Integer patch - add to_rom method
module IntegerPatch
  refine Integer do
    def to_rom
      Roman.new(self)
    end

    def eql?(other)
      if other.class == Roman
        self == other.value
      end

      self == other
    end
  end
end