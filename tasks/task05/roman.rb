require_relative 'roman_factory'

MAX_VALUE = 3000
MIN_VALUE = 1

# Roman number representation
class Roman
  include Comparable, RomanFactory

  attr_accessor :value
  alias :to_int :value

  def initialize(number)
    if number.is_a? String
      parsed = string_to_number(number)
      raise ArgumentError 'wrong number value' unless valid_number?(parsed)
      @value = parsed
    elsif number.is_a? Integer
      raise ArgumentError 'wrong number value' unless valid_number?(number)
      @value = number
    else
      ArgumentError 'Invalid argument'
    end

  end

  def <=>(other)
    value <=> other
  end

  def +(other)
    value + other
  end

  def -(other)
    value - other
  end

  def *(other)
    value * other
  end

  def /(other)
    value / other
  end

  def coerce(other)
    [other, @value]
  end

  def eql?(other)
    @value == other
  end

  def to_s
    number_to_string(value)
  end

  private

  def valid_number?(number)
    return false unless number.is_a? Integer
    number.between?(MIN_VALUE, MAX_VALUE)
  end

end