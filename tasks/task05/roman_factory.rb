require_relative 'roman'

# code from:
# https://www.geeksforgeeks.org/converting-decimal-number-lying-between-1-to-3999-to-roman-numerals/
# https://www.geeksforgeeks.org/converting-roman-numerals-decimal-lying-1-3999/
NUM = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000].freeze
SYM = %w[I IV V IX X XL L XC C CD D CM M].freeze

module RomanFactory

  def string_to_number(str_value)
    res = 0
    array = str_value.split('')

    index = 0
    while index < array.size
      s1 = char_to_int(array[index])

      if index + 1 < array.size
        s2 = char_to_int(array[index + 1])

        if s1 >= s2
          res += s1
        else
          res += s2 - s1
          index += 1
        end
      else
        res += s1
      end

      index += 1
    end

    res
  end

  def number_to_string(number)
    index = 12
    res = ''
    while number.positive?
      div = number / NUM[index]
      number = number % NUM[index]
      while div.positive?
        res += SYM[index]
        div -= 1
      end
      index -= 1
    end
    res
  end

  private

  def char_to_int(char)
    index = SYM.index(char)
    NUM[index]
  end

end