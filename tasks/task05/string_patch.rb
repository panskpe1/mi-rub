require_relative './roman'

# String patch - add to_rom method
module StringPatch
  refine String do
    def to_rom
      Roman.new(self)
    end
  end
end