class Horse
  include Comparable

  attr_accessor :value

  def initialize(value)
    self.value = value
  end

  def +(other)
    other + value
  end

  # Coerce s prohozenym poradim parametru u metody (1..horse), zpusobal, ze metoda volala jako (horse..1)
  def coerce(other)
    [Horse.new(other), self]
  end

  # Metoda je potreba pro porovnani s integerem
  def <=>(other)
    value <=> other.value
  end
end


horse = Horse.new(42)
pony = Horse.new(11)

puts horse + 22
puts horse + pony
puts 11 + horse
puts [horse, pony].sum

# Nyni je mozne vytvorit sekvenci z cisla a kone
puts (1..Horse.new(10)).to_a