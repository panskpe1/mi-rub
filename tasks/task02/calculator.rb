class Calculator
  attr_writer :name
  attr_reader :operations_count

  def initialize(name)
    @name = name
    @operations_count = 0
  end

  def add(*numbers)
    increment_operations_count
    numbers.sum
  end

  def subtract(number1, number2)
    increment_operations_count
    number1 - number2
  end

  def multiply(number1, number2)
    increment_operations_count
    number1 * number2
  end

  def divide(number, divider)
    if divider == 0
       raise ArgumentError, 'Cannot divide by zero!'
    end
    increment_operations_count
    number / divider.to_f
  end

  def name
    @name.upcase
  end

  class << self
    def extreme(type, *numbers)
      if type != :max && type != :min
        raise ArgumentError, 'Wrong extreme type'
      end
      numbers.send(type)
    end

    def number?(number)
      number.is_a? Numeric
    end
  end

  private

  def increment_operations_count
    @operations_count += 1
  end
end
