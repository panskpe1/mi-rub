require "test/unit"

class Calculator
  attr_writer :name
  attr_reader :operations_count

  def initialize(name)
    @name = name
    @operations_count = 0
  end

  def add(*numbers)
    increment_operations_count
    numbers.sum
  end

  def subtract(number1, number2)
    increment_operations_count
    number1 - number2
  end

  def multiply(number1, number2)
    increment_operations_count
    number1 * number2
  end

  def divide(number, divider)
    if divider == 0
       raise ArgumentError, 'Cannot divide by zero!'
    end
    increment_operations_count
    number / divider.to_f
  end

  def name
    @name.upcase
  end

  class << self
    def extreme(type, *numbers)
      if type != :max && type != :min
        raise ArgumentError, 'Wrong extreme type'
      end
      numbers.send(type)
    end

    def number?(number)
      number.is_a? Numeric
    end
  end

  private

  def increment_operations_count
    @operations_count += 1
  end
end


# Test cases 
class TestSimpleNumber < Test::Unit::TestCase

  def setup
    @calc = Calculator.new('CalC')
  end

  def test_getter_setter
    assert_equal('CALC', @calc.name)
    @calc.name = 'NewName'
    assert_equal('NEWNAME', @calc.name)
  end

  def test_add
    assert_equal(10, @calc.add(1, 2, 3, 4))
    assert_equal(0, @calc.add())
    assert_equal(0, @calc.add(-10, 5, 5))
  end

  def test_minus
    assert_equal(8, @calc.subtract(10, 2))
    assert_equal(0, @calc.subtract(10, 10))
  end

  def test_multiply
    assert_equal(24, @calc.multiply(6, 4))
    assert_equal(0, @calc.multiply(0, 4))
    assert_equal(0, @calc.multiply(4, 0))
  end

  def test_divide
    assert_in_delta(5, @calc.divide(10, 2), 0.00001)
    assert_in_delta(2.5, @calc.divide(10, 4), 0.00001)
    assert_in_delta(3.33333, @calc.divide(10, 3), 0.00001)
  end

  def test_divide_by_zero
    assert_raises do
      @calc.divide(10, 0)
    end
  end

  def test_extreme_min
    assert_equal(1, Calculator.extreme(:min, 10, 4, 3, 1))
    assert_equal(-3, Calculator.extreme(:min, 10, -3, 4, 3, 1))
  end

  def test_extreme_max
    assert_equal(10, Calculator.extreme(:max, 10, 4, 3, 1))
    assert_equal(23, Calculator.extreme(:max, -3, 23, 4, 3, 1))
  end

  def test_extreme_unknown_type
    assert_raises do
      Calculator.extreme(:minimum, 10, 4, 3, 1)
    end
  end

  def test_valid_number
    assert_true(Calculator.number?(1))
    assert_true(Calculator.number?(100.23))
  end

  def test_invalid_number
    assert_false(Calculator.number?('String'))
    assert_false(Calculator.number?(nil))
  end

  def test_operations_count
    calc2 = Calculator.new('second')

    assert_equal(0, @calc.operations_count)
    @calc.add(1, 2, 3)
    calc2.add(1, 2, 3)
    @calc.add(1)
    @calc.subtract(4, 2)
    calc2.subtract(4, 2)
    @calc.divide(4, 2)

    assert_equal(4, @calc.operations_count)
    assert_equal(2, calc2.operations_count)

    @calc.add(1, 2, 3)
    calc2.add(1, 2, 3)
    @calc.multiply(4, 2)
    @calc.multiply(9, 3)

    assert_equal(7, @calc.operations_count)
    assert_equal(3, calc2.operations_count)
  end
end
