(1..20).each do |x|
  val = ''
  if x % 3 == 0
    val += "Fizz"
  end

  if x % 5 == 0
    val += "Buzz"
  end

  puts (val.empty?)? x : val
end