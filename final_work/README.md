
# README  
  
## SportSocial  

It's Rails App with backend and views that are generated from controllers.
  
User can:  
- Signin
- Signup  
- Reset the forgotten password  
- Upload new activity  
- Edit user's activities  
- Show detail of every activity  
- Follow users  
- Give kudos to the concrete activity  
- Comment activities on the detail page  
- Show dashboard with user's activities or user's and following user's activities   
- Show dashboard with user's and following user's activities  
  
### How to run app: 
Init database and store gpx files into Active Storage.  Therefore sometimes it can take longer than classic seed.
```  
rake db:setup    
``` 
Now you can run app:  
```  
rails s  
```  
  
There are registered users and created activities that are defined in the file `db/seeds.rb`.
Precreated users:  
- user1@sport.cz  
- user2@sport.cz 
- user3@sport.cz  
- user4@sport.cz  
- user5@sport.cz  
  
All users have same passwords to make login easy.  
```
password: 'aaaaaa' (6x letter 'a')
```
 
If you forget the password, you can reset it. After the form is submitted, there is sent an email with steps to reset. Instead of sending real email, an email is generated into the directory: `tmp/mail`.
 
    
Used images on the home page are from:   
- Ride image: http://www.ceskasporitelna-accolade.cz/files/uploads/%C3%BAvod/4-uvod.jpg  
- Run image: https://runningschool.com/wp-content/uploads/2017/10/5km-21km-1.jpg