class CreateActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :activities do |t|
      t.string :name, null: false
      t.text :description
      t.float :distance, null: false
      t.integer :duration, null: false
      t.datetime :start_at, null: false

      t.references :user, null: false
      t.references :activity_type, null: false

      t.boolean :active, null: false, default: true

      t.timestamps
    end
  end
end
