class CreateActivityTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :activity_types do |t|
      t.string :name, null: false

      t.timestamps
    end
    add_index :activity_types, :name, unique: true
  end
end
