# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.create(email: 'user1@sport.cz', password: 'aaaaaa',  password_confirmation: 'aaaaaa', first_name: 'Joe', last_name: 'Lastname 1')
user2 = User.create(email: 'user2@sport.cz', password: 'aaaaaa', password_confirmation: 'aaaaaa', first_name: 'Monica', last_name: 'Lastname 2')
user3 = User.create(email: 'user3@sport.cz', password: 'aaaaaa', password_confirmation: 'aaaaaa', first_name: 'Ross', last_name: 'Lastname 3')
user4 = User.create(email: 'user4@sport.cz', password: 'aaaaaa', password_confirmation: 'aaaaaa', first_name: 'Chandler', last_name: 'Lastname 4')

Follower.create(follower_id: user1.id, user_id: user2.id)
Follower.create(follower_id: user1.id, user_id: user3.id)
Follower.create(follower_id: user4.id, user_id: user1.id)

ride = ActivityType.create(name: 'Ride')
run = ActivityType.create(name: 'Run')
walk = ActivityType.create(name: 'Walk')


def activity_with_gpx_data(activity, filename, extension = 'gpx')
  fullname = "#{filename}.#{extension}"
  filepath = Rails.root.join("gpx_samples/#{fullname}")

  activity.activity_file.attach(io: File.open(filepath), filename: fullname)

  gpx_service = GpxService.new
  gpx_service.update_activity_with_gpx(activity, File.open(filepath))
end

p 'Creating rides'
ride1 = Activity.new(name: 'Ride 1', user_id: user1.id, activity_type_id: ride.id)
activity_with_gpx_data(ride1, 'ride1').save!

ride1.activity_kudos.create(user_id: user2.id)
ride1.activity_kudos.create(user_id: user3.id)
ride1.comments.create(commenter_id: user2.id, body: 'Good!')
ride1.comments.create(commenter_id: user1.id, body: 'Thx!')

ride2 = Activity.new(name: 'Ride 2', user_id: user1.id, activity_type_id: ride.id)
activity_with_gpx_data(ride2, 'ride2').save!

ride3 = Activity.new(name: 'Ride 3', user_id: user2.id, activity_type_id: ride.id)
activity_with_gpx_data(ride3, 'ride3').save!

ride4 = Activity.new(name: 'Ride 4', user_id: user3.id, activity_type_id: ride.id)
activity_with_gpx_data(ride4, 'ride4').save!

p 'Creating runs'
run1 = Activity.new(name: 'Run 1', user_id: user2.id, activity_type_id: run.id)
activity_with_gpx_data(run1, 'run1').save!

run2 = Activity.new(name: 'Run 2', user_id: user2.id, activity_type_id: run.id)
activity_with_gpx_data(run2, 'run2').save!

run3 = Activity.new(name: 'Run 3', user_id: user4.id, activity_type_id: run.id)
activity_with_gpx_data(run3, 'run3').save!

p 'Creating walks'
walk1 = Activity.new(name: 'Walk 1', user_id: user1.id, activity_type_id: walk.id)
activity_with_gpx_data(walk1, 'walk1').save!


removed1 = Activity.new(name: 'Removed ride', user_id: user1.id, activity_type_id: ride.id, active: false)
activity_with_gpx_data(removed1, 'ride2').save!

removed2 = Activity.new(name: 'Removed ride', user_id: user2.id, activity_type_id: ride.id, active: false)
activity_with_gpx_data(removed2, 'ride3').save!
