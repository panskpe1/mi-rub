class Comment < ApplicationRecord
  validates_presence_of :body

  belongs_to :commenter, class_name: :User
  belongs_to :activity
end
