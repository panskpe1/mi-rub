class Activity < ApplicationRecord
  # Activity file is stored in the active storage
  has_one_attached :activity_file

  belongs_to :user
  belongs_to :activity_type

  has_many :comments
  has_many :activity_kudos
  has_many :kudoers, through: :activity_kudos, source: :user

  # By default is visible only activities that have active flat set to true
  default_scope { where(active: true).order(start_at: :desc) }
end
