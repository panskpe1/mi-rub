class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates_presence_of :first_name, :last_name

  has_many :followers_conn, class_name: :Follower, foreign_key: :user_id
  has_many :following_users_conn, class_name: :Follower, foreign_key: :follower_id

  has_many :followers, through: :followers_conn, source: :follower
  has_many :following_users, through: :following_users_conn, source: :user

  has_many :activities

  def fullname
    "#{first_name} #{last_name}"
  end
end
