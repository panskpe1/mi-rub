class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = User.all
  end

  # User profile page with information about follower/following users and total kilometers according to the activities
  def profile
    @user = User.find(params[:id])
    @my_profile = current_user == @user

    distances = Activity.where(user_id: current_user.id).group(:activity_type_id).sum(:distance)

    @distances = ActivityType.all.map do |type|
      dist = 0
      if distances.include?(type.id)
        dist = distances[type.id]
      end

      { type: type, distance: dist }
    end
  end
end
