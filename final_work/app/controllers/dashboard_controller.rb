# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :authenticate_user!

  TYPES = {
    following: {
      key: 'following',
      title: 'Following'
    },
    your_activities: {
      key: 'your_activiies',
      title: 'Your activities'
    }
  }.freeze

  DEFAULT_KEY = TYPES[:following][:key]

  # Dashboard activities
  def index
    @editable = true
    @selectable = TYPES.map { |_, value| [value[:title], value[:key]] }

    @type = params[:type]
    @type = DEFAULT_KEY if @type.nil?

    if @type == TYPES[:following][:key]
      user_ids = current_user.following_users.map(&:id)
      user_ids << current_user.id

      @activities = Activity.where(user_id: user_ids)
    elsif @type == TYPES[:your_activities][:key]
      @activities = current_user.activities
    else
      @activities = []
    end
  end
end
