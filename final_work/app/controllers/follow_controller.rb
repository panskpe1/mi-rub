class FollowController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:follow, :unfollow]

  # Start to follow the user
  def follow
    if @user != current_user
      found = find_follow_relation(current_user, @user)
      if found.nil?
        Follower.create(follower_id: current_user.id, user_id: @user.id)
      end
    end

    respond_to do |format|
      format.js { render inline: 'location.reload();' }
    end
  end

  # Stop to follow the user
  def unfollow
    found = find_follow_relation(current_user, @user)
    found.destroy unless found.nil?

    respond_to do |format|
      format.js { render inline: 'location.reload();' }
    end
  end

  private
    def find_follow_relation(follower, user)
      Follower.find_by(follower_id: follower.id, user_id: user.id)
    end

    def set_user
      @user = User.find(params[:id])
    end
end
