class KudosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_activity

  # Give a kudos to the owner of the activity. There is no change to cancel the kudo.
  def give_kudos
    if @activity.user != current_user
      found = ActivityKudo.find_by(user: current_user, activity: @activity)
      ActivityKudo.create(user: current_user, activity: @activity) if found.nil?
    end

    respond_to do |format|
      format.js { render inline: 'location.reload();' }
    end
  end

  private
    def set_activity
      @activity = Activity.find(params[:id])
    end
end
