class ActivitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_activity, only: [:show, :edit, :update, :destroy, :activity_points]
  before_action :require_owner, only: [:edit, :update, :destroy]

  # List of activities
  def index
    @activities = Activity.all
    @title = 'All activities'
    @editable = false
  end

  # Activities of the current logged user
  def user_activities
    @activities = Activity.where(user: current_user)
    @title = 'My activities'
    @editable = true
    render 'activities/index'
  end

  # Detail of the activity
  def show
    @given_kudos = !ActivityKudo.find_by(user: current_user, activity: @activity).nil?
  end

  # Return json with activity's track points
  def activity_points
    content = @activity.activity_file.download
    gpx_service = GpxService.new
    @points = gpx_service.get_points(content).map { |p| [p.lon, p.lat] }
    render json: @points
  end

  def new
    @activity = Activity.new
  end

  # After uploading create a new activity
  def create
    gpx_service = GpxService.new
    @activity = Activity.new(activity_params)
    @activity.user = current_user

    file_to_storage = @activity.attachment_changes['activity_file'].attachable
    gpx_service.update_activity_with_gpx(@activity, file_to_storage)

    if @activity.save!
      flash[:success] = 'Activity was successfully uploaded.'
      redirect_to @activity
    else
      render :new
    end
  rescue StandardError
    flash[:notice] = 'Uploaded file was not in the format GPX.'
    render :new
  end

  def edit; end

  # Update some information of the activity
  def update
    if @activity.update(activity_params)
      flash[:success] = 'Activity was successfully updated.'
      redirect_to @activity
    else
      render :edit
    end
  end

  # Remove the activity. There is only set a flag 'active' to false
  def destroy
    @activity.active = false
    @activity.save

    flash[:success] = 'Activity was successfully deleted.'
    redirect_to user_activities_path
  end

  private
    # Find and set activity
    def set_activity
      @activity = Activity.find(params[:id])
    end

    # Check that the current user is the owner
    def require_owner
      if current_user.id != @activity.user_id
        flash[:notice] = "You don't have permissions to perform this action."
        redirect_to activities_path
      end
    end

    def activity_params
      params.require(:activity).permit(:name, :description, :activity_file, :activity_type_id)
    end
end
