class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_activity
  before_action :require_commenter, only: [:destroy]

  # Create new comment
  def create
    @comment = @activity.comments.new(comment_params)
    @comment.commenter = current_user
    @comment.save!
    redirect_to activity_path(@activity)
  end

  # Remote the comment
  def destroy
    @comment = @activity.comments.find(params[:id])
    if [@comment.commenter, @activity.user].include?(current_user)
      @comment.destroy
      flash[:success] = 'A comment was removed.'
    else
      flash[:notice] = 'You cannot remove this comment!'
    end
    redirect_to activity_path(@activity)
  end

  private
    def comment_params
      params.require(:comment).permit(:body)
    end

    def set_activity
      @activity = Activity.find(params[:activity_id])
    end

    def require_commenter
      if current_user.id != @activity.commenter_id
        flash[:notice] = "You don't have permissions to perform this action."
        redirect_to activity_path(@activity)
      end
    end
end
