module ActivitiesHelper
  DATE_FORMAT = '%d.%m.%Y %R'.freeze
  TIME_FORMAT = '%H:%M:%S'.freeze

  # Activity types for selectboxes
  def selectable_activity_types
    ActivityType.all.map { |f| [f.name, f.id] }
  end

  # Show readable date
  def human_date(date)
    if date.nil?
      ''
    else
      date.strftime(DATE_FORMAT)
    end
  end

  # Show readable time
  def human_time(seconds)
    Time.at(seconds).utc.strftime(TIME_FORMAT)
  end

  # Show readable distance
  def human_distance(kilometers)
    "#{kilometers.round(2)} km"
  end

  # Class for the kudos button.
  def kudos_class(given_kudos)
    given_kudos ? 'btn btn-dark' : 'btn btn-outline-dark'
  end

  # Title for the kudos button.
  def kudos_title(given_kudos)
    given_kudos ? 'Kudos given' : 'Give kudos'
  end
end
