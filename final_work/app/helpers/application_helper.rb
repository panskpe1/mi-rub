module ApplicationHelper
  # Map flash types to responding html classes
  # Source code: https://coderwall.com/p/jzofog/ruby-on-rails-flash-messages-with-bootstrap
  def flash_class(level)
    case level
    when 'notice' then
      'alert alert-info'
    when 'success' then
      'alert alert-success'
    when :success then
      'alert alert-success'
    when 'error' then
      'alert alert-danger'
    when 'alert' then
      'alert alert-danger'
    else
      'alert'
    end
  end

  # Show inline error, if exists
  def inline_errors(model, model_attribute)
    result = ''
    if model.errors[model_attribute].any?
      model.errors[model_attribute].each do |message|
        result += "<li>#{message}</li>"
      end
    end
    "<ul>#{result}</ul>".html_safe
  end

  # Generate a field to show errors
  def field_error_div(model, model_attribute)
    ('<div class="error text-danger">' +
        inline_errors(model, model_attribute) +
        '</div>').html_safe
  end
end
