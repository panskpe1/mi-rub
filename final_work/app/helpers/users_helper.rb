module UsersHelper
  # Generate link to the user profile
  def user_link(user)
    link_to user.fullname, user_profile_path(user)
  end

  # Generate the button to follow or unfollow the user according to the given data
  def follow_unfollow_btn(user, user_to_follow)
    if user == user_to_follow
      nil
    elsif user.following_users.include?(user_to_follow)
      link_to(
        button_tag('Unfollow', class: 'btn btn-primary'),
          unfollow_user_path(user_to_follow),
          method: :put,
          remote: true
      )
    else
      link_to(
        button_tag('Follow', class: 'btn btn-primary'),
          follow_user_path(user_to_follow),
          method: :put,
          remote: true
      )
    end
  end
end
