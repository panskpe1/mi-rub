module CommentsHelper
  # Is the comment deletable by the current_user.
  def can_delete_comment?(comment, current_user)
    [comment.commenter, comment.activity.user].include?(current_user)
  end
end
