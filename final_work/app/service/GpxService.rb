# Service that works with gpx data
class GpxService
  # Get the array of track points objects from the the content.
  # Content is according to the gpx schema definition.
  def get_points(gpx_content)
    gpx = GPX::GPXFile.new(gpx_data: gpx_content)

    return [] if gpx.tracks.empty? || gpx.tracks[0].segments.empty?

    gpx.tracks[0].segments[0].points
  end

  # Update information in the activity according to the gpx_file.
  # Updated are only attributes that can be calculated from the gpx file.
  def update_activity_with_gpx(activity, gpx_file)
    gpx = GPX::GPXFile.new(gpx_file: gpx_file)

    activity.distance = gpx.distance
    activity.duration = gpx.duration
    activity.start_at = gpx.time

    activity
  end
end
