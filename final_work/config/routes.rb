Rails.application.routes.draw do
  get 'follow/follow'
  get 'follow/unfollow'

  get 'activities/user', to: 'activities#user_activities', as: 'user_activities'

  get 'activities/:id/points', to: 'activities#activity_points', as: 'activity_points'
  put 'activities/:id/kudos', to: 'kudos#give_kudos', as: 'give_kudos'
  resources :activities do
    resources :comments
  end

  devise_for :users
  root to: "welcome#home"

  get '/users', to: 'users#index'
  get '/profile/:id', to: 'users#profile', as: 'user_profile'
  get '/dashboard', to: 'dashboard#index'
  put '/follow/:id', to: 'follow#follow', as: 'follow_user'
  put '/unfollow/:id', to: 'follow#unfollow', as: 'unfollow_user'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
